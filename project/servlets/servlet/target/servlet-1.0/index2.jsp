<%@ page errorPage="errorPage.jsp" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<%
	String str1 = "Lukasz";
%>

<jsp:scriptlet>
	String str2 = "Lukasz";
</jsp:scriptlet>

<%!
	String str3;
%>

<jsp:declaration>
	String str4;
</jsp:declaration>

<%= "Ala ma kota" %>

<jsp:expression>
	"a kot ma ale"
</jsp:expression>

<!-- Tego nie powinno byc widac -->

<%
	int a = 5;
	int b = 0;
%>

</body>
</html>