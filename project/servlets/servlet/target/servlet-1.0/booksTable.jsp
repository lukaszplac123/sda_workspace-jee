<%@page import="book.model.BookRepository"%>
<%@page import="java.util.List"%>
<%@page import="book.model.Book"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Books library</title>
</head>
<body>

<jsp:useBean id="booksRepo" type="book.model.BookRepository" scope="application"/>
<c:set var="booksList" scope="application" value="${booksRepo.booksList}"/>

<form method="GET" action="addAction.jsp">
	<button type="submit" value="perform" name="add">Add new Book</button>
</form>

<table style="width:80%; text-align: center;">
  <tr>
  	<th>Number</th>
    <th>Title</th> 
    <th>Author</th>
    <th>Pages</th>
    <th>Date</th>
    <th></th>
  </tr>
  
  <c:forEach items="${applicationScope.booksList}" var="book" varStatus="count">

  	<tr>
  	<td><c:out value="${count.index}"/></td>
    <td><c:out value="${book.bookName}"/></td> 
    <td><c:out value="${book.bookAuthor}"/></td>
    <td><c:out value="${book.numberOfPages}"/></td>
    <td><fmt:parseDate type="date" value="${book.date}" pattern="yyyy-MM-dd" var="parsedDate"/>
    	<fmt:formatDate value="${parsedDate}" pattern="dd.MM.yyyy" type="date"/></td>
	<td>
	    	<form method="GET" action="removeAction.jsp" style="float:left;">
	    		<button type="submit" value="${count.index}" name="remove">Rem</button>
	    	</form>
	    	<form method="GET" action="updateAction.jsp">
	    		<button type="submit" value="${count.index}" name="update">Upd</button>
	    	</form>
	</td>
    </tr>
  </c:forEach>
</table> 
  
</body>
</html>