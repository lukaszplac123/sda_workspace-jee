<%@page import="app.UsersRepository"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="bean.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<jsp:useBean id="user1" class="bean.User" scope="session" >
    <jsp:setProperty name="user1" property="name"
                    value="Lukasz"/>
    <jsp:setProperty name="user1" property="age"
                    value="18"/>
</jsp:useBean>

<jsp:useBean id="user2" class="bean.User" scope="session" >
    <jsp:setProperty name="user2" property="name"
                    value="Radek"/>
    <jsp:setProperty name="user2" property="age"
                    value="22"/>
</jsp:useBean>

<%
	List<User> users = new ArrayList<User>();
		users.add(user1);
		users.add(user2);
%>


<jsp:element name="Users">
		<jsp:attribute name="count"></jsp:attribute>
		<jsp:body>
			<%for (int i = 0 ; i < users.size(); i ++){%>
			<jsp:element name="User">
				<jsp:body>
					<jsp:element name = "Name">
							<jsp:body>
							<%out.write(users.get(i).getName()); %>
							</jsp:body>
					</jsp:element>
					<jsp:element name = "Age">
							<jsp:body>
							<%out.write(users.get(i).getAge()); %>
							</jsp:body>
					</jsp:element>
				</jsp:body>
			</jsp:element>
			<%} %>
		</jsp:body>
</jsp:element>

</body>
</html>