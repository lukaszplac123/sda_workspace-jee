<%@page import="bean.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:useBean id="user1" class="bean.User" scope="session" >
	<%
		if (user1 != null){
			out.write("bean not null\n");
		}
		if (user1 instanceof User){
			out.write("bean is instance of User\n");
		}
	%>	
    <jsp:setProperty name="user1" property="name"
                    param="name"/>
    <jsp:setProperty name="user1" property="age"
                    param="age"/>
     
    <p>name:</p>                
    <jsp:getProperty name="user1" property="name"/>
    <p>age:</p>  
    <jsp:getProperty name="user1" property="age"/>
</jsp:useBean>

<%
	

%>
</body>
</html>