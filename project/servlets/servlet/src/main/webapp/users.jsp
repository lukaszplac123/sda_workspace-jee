<%@page import="app.UsersRepository"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Set"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h1>Users list:</h1>
<%	
	UsersRepository users = (UsersRepository) getServletContext().getAttribute("users");
	if (users.getUsersSet() != null){
		String[] array = new String[users.getUsersSet().size()];
		users.getUsersSet().toArray(array);
		int length = array.length;
		for (int i = 0; i < length; i++){
			out.write (array[i]);
		}
	} else {
		out.write("No users in the list");
	}
%>
</body>
</html>