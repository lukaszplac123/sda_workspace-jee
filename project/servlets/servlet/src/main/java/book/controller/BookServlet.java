package book.controller;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.UsersRepository;
import book.model.Book;
import book.model.BookRepository;

/**
 * Servlet implementation class BookServlet
 */
@WebServlet("/BookServlet")
public class BookServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    public BookServlet() {
        super();

    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		BookRepository booksRepository = (BookRepository) getServletContext().getAttribute("booksRepo");		
		Book newBook = (Book) request.getSession(false).getAttribute("book");
		Map<String, String[]> paramMap = request.getParameterMap();
			if ((paramMap.containsKey("bookName"))
					&& (paramMap.containsKey("bookAuthor"))
					&& (paramMap.containsKey("date")) 
					&& (paramMap.containsKey("pages"))){
				booksRepository.addBook(newBook);
			} else {
				
			}
		response.sendRedirect("booksTable.jsp");
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
