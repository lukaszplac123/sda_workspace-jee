package params;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Servlet15")
public class Servlet15 extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Map<String, String[]> paramMap = req.getParameterMap();
		
		//metoda 1 - pobraie mapy parametrow
		String[] paramValues = null;
		if (paramMap.containsKey("param0")){
			paramValues = paramMap.get("param0");
		}
		resp.getWriter().println("Welcome in server 15");
		if (paramValues != null){
	
		//metoda 2 - uzycie enumeratora
		Enumeration<String> paramEnumerator = req.getParameterNames();
		String[] paramValues1 = null;
		while (paramEnumerator.hasMoreElements()){
			String parameter = paramEnumerator.nextElement();
			if (parameter.equals("param0")){
				paramValues1 = req.getParameterValues("param0");
			}
		}
		
		//wyswietlanie obydwu
		StringBuilder builder1 = new StringBuilder();
		StringBuilder builder2 = new StringBuilder();
		for (String str : paramValues){
			builder1.append(str+",");
		}
		for (String str : paramValues1){
			builder2.append(str+",");
		}	
		if (paramValues != null & paramValues1 != null)
		resp.getWriter().println("Consumed parameter param0 : " + builder1.toString());
		resp.getWriter().println("Consumed parameter param0 : " + builder2.toString());
		} else{
			resp.getWriter().println("No parameters received");
		}
	}
}
