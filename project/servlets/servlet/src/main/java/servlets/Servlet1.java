package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Servlet1
 */
@WebServlet(value = "/Servlet1", initParams = @WebInitParam(name = "myParam0", value = "dfffa"))
public class Servlet1 extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
    private int counter;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Servlet1() {
        super();
        counter = 0;
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		out.println("<h1>"+getServletContext().getInitParameter("name")+"</h1>");
		out.println("<h1>Servlet 1. Welcome</h1>");
		out.println("<h2>Counter : " + counter++ + "</h2>");
		resp.sendRedirect("Servlet2");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
