package app;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class UsersRepository {
	
	private Set<String> usersSet;
	
	public Set<String> getUsersSet() {
		return usersSet;
	}

	public UsersRepository(){
		Set<String> tempSet = new HashSet<String>();
		
		//chcemy aby set byl sychronizowany pomiedzy watkami serwera
		usersSet = Collections.synchronizedSet(tempSet);
	}
	
	public void addName(String name){
		usersSet.add(name);
	}
}
