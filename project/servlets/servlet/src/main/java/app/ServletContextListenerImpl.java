package app;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ServletContextListenerImpl implements ServletContextListener {
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		sce.getServletContext().setAttribute("users", new UsersRepository());
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		
	}

}
