package app;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/Controller")
public class Controller extends HttpServlet{

	
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		//zamiast listenera ServletContextListenerImpl
		//mozna by zrobic synchronizacje blokowa i synchronizowac server kontekst
		//synchronized(ServerContext)
		
		UsersRepository usersRepository = (UsersRepository) getServletContext().getAttribute("users");
		Map<String, String[]> paramMap = req.getParameterMap();
		if (usersRepository.getUsersSet().isEmpty()){
			if (paramMap.containsKey("name")) {
				usersRepository.addName(req.getParameter("name"));
			} else {}
		} else{
			usersRepository.getUsersSet().add(req.getParameter("name"));
			}	
		resp.sendRedirect("users.jsp");
	}
}
