package sessions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Session")
public class SessionTest extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.getWriter().println("Welcome in Session servlet");
		Cookie[] cookies = req.getCookies();
		for (Cookie cookie : cookies){
			resp.getWriter().println(cookie.getName());
		}
		//po pierwszym uruchomieniu servletu nie widzimy cookie "JSESSIONID" gdyz nie ma jeszcze sesji. Sesja jest
		//rozpoczynana po p�tli for dlatego gdy odswiezamy strone i uruchamiamy servlet jeszcze raz, sesja juz istnieje
		//dzieki czemu na ekran wypisywany jest "JSESSIONID" cookie
		//po odswiezeniu strony 
		req.getSession(true);
	}
}
