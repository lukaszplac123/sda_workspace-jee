package sessions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Servlet31")
public class Servlet31 extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getSession(true);
		resp.getWriter().println(req.getAttribute("lukiAttr1"));
		resp.getWriter().println(req.getAttribute("lukiAttr2"));
		resp.getWriter().println(req.getAttribute("lukiAttr3"));
	}
}
