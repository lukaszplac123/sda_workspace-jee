package sessions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Servlet30")
public class Servlet30 extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getSession(true);
		req.getSession().setAttribute("lukiAttr1", "attr1");
		req.getSession().setAttribute("lukiAttr2", "attr2");
		req.setAttribute("lukiAttr3", "attr3");
		req.getRequestDispatcher("Servlet31").include(req, resp);
	}
}
