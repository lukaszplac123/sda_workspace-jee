package ejbs;

import javax.ejb.Local;

@Local
public interface TempEjb {
	public void say();
}
