package myEjb;

import java.io.Serializable;
import java.rmi.RemoteException;

import javax.ejb.Local;

@Local
public interface TestRemote{
	String say();
}
