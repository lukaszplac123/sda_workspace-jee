<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>Witaj!</h1>
    <h2>Zaloguj do panelu administratora</h2>
    <form action="AdminServletRedir" method="post">
        <input type="text" name="username">
        <input type="submit" value="Zaloguj (Redirection)">
    </form>
    <br>
    <form action="AdminServletForw" method="post">
        <input type="text" name="username">
        <input type="submit" value="Zaloguj (Forward)">
    </form>
    <br>
    <form action="AdminServletIncl" method="post">
        <input type="text" name="username">
        <input type="submit" value="Zaloguj (Include)">
    </form>
</body>
</html>